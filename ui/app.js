async function getOccupantInformation() {
    let adress = document.getElementById("addressField").value
    let url = "/api/v1/occupant-information/?address="+adress;
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
        return false;
    }
}

async function renderOccupantInformation() {
    let occupantInformation = await getOccupantInformation();
    console.log(occupantInformation)
    let html = '';
    let htmlSegment;
    if (typeof(occupantInformation) === 'object' && occupantInformation.occupant_information_id) {
         htmlSegment = `<div class="occupantInformation">
        <span>Aantal bewoners: ${occupantInformation.occupants}</span><br/>
        <span>Locatie gasfles: ${occupantInformation.gas_cannister_location}</span>
    </div>`;
    } else {
            htmlSegment = `<div class="occupantInformation">
                        <span style="color: red;">Resultaat: Geen toegang tot bewonersinformatie </span><br/>
                    </div>`;
    }

    html += htmlSegment;

    let container = document.querySelector('.occupantInformationList');
    container.innerHTML = html;
}
